#!/usr/bin/env python
# coding=utf-8
"""
lasergcode: Inkscape extension to generate gcode for a 3D printer fitted with a pen.
Copyright 2017 Olly F-G olly@ollyfg.com
Based on lasergcode.py by Greg Fawcett (https://bitbucket.org/vig/lasergcode).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# Update id tag in inx file when this changes.
lasergcode_current_version = "1,0"
debug = False

import inkex
from inkex import bezier

#-----------------------------------------------------------------------
# Constants that should probably be settings
preamble = [
	'G90; Absolute mode',
	'G21; Millimeters',
	'G92 X0 Y0; Set current position as XY home'
]
postamble = ['G0 X0 Y0; Return to home position']

#-----------------------------------------------------------------------
def element_viewbox_transform(e, width_mm, height_mm):
	"""
	Get element viewbox transform.
	"""
	try:
		vb_str=e.get('viewBox')
		vx, vy, vwidth, vheight=(float(i) for i in vb_str.split())
		xratio = width_mm/vwidth
		yratio = height_mm/vheight
		return inkex.Transform([[xratio, 0.0, -vx*xratio], [0.0, yratio, -vy*yratio]])
	except:
		return inkex.Transform([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]])

#-----------------------------------------------------------------------
def element_transform(e):
	"""
	Get element transform, or return the identity transform.
	"""
	transform_str=e.get('transform')
	if transform_str:
		return inkex.Transform(transform_str)
	return inkex.Transform([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]])

#-----------------------------------------------------------------------
def transform_str(t):
	"""
	Return a string describing a transform.
	"""
	return '[[%f, %f, %f],[%f, %f, %f]]'%(t[0][0], t[0][1], t[0][2], t[1][0], t[1][1], t[1][2])


#=======================================================================
class SvgPlotterEffect(inkex.OutputExtension):

	#-----------------------------------------------------------------------
	def __init__(self):
		inkex.Effect.__init__(self)
		self.arg_parser.add_argument('--zhop_height',
			type = int, dest = 'zhop_height', default = '5', help = 'zHop Height (mm)')
		self.arg_parser.add_argument('--speed',
			type = int, dest = 'speed', default = '100', help = 'Speed (mm/second)')
		self.gcode=[]

	#-----------------------------------------------------------------------
	def recurse_group(self, group, parent_transform):
		"""
		Get gcode for any paths in group children, including recursing child groups.
		"""
		for child in group:
			try:
				etype = child.tag[child.tag.index('}')+1:] # Everything after the closing "}"
			except:
				inkex.debug('Ignoring element '+child.tag)
				continue

			# Recurse into child elements of group
			if etype=='g':
				child_transform=parent_transform * element_transform(child)
				self.recurse_group(child, child_transform)

			# Add gcode for the path to self.gcode
			if etype=='path':
				child_transform=parent_transform * element_transform(child)
				# Convert SVG path data into sets of subpaths of cubic spline points.
				super_path = inkex.CubicSuperPath(child.get('d'))
				# Transform cubic spline points.
				for sub_path in super_path:
					for point_set in sub_path:
						for point in point_set:
							point[0] = child_transform.matrix[0][0]*point[0] + child_transform.matrix[0][1]*point[1] + child_transform.matrix[0][2]
							point[1] = child_transform.matrix[1][0]*point[0] + child_transform.matrix[1][1]*point[1] + child_transform.matrix[1][2]
					# Subdivide curves.
					bezier.subdiv(sub_path, 0.2)
					raised = True
					for point_set in sub_path:
						x, y = point_set[1][0], point_set[1][1]
						if (0 <= x <= self.width) and (0 <= y <= self.height):
							self.gcode.append('G0 X%0.1f Y%0.1f'%(x, y))
							if raised:
								self.gcode.append('G0 Z0; Lower') # Lower head
								raised = False
						else:
							inkex.debug('Point %0.2f, %0.2f is outside drawing - ignored.'%(x, y))
					if not raised:
						self.gcode.append('G0 Z%d; Raise'%(self.options.zhop_height)) # Raise head

	#-----------------------------------------------------------------------
	def effect(self):
		preamble.append('G0 F%d; Set speed'%(self.options.speed * 60)) # Input speed is mm/s, gcode needs mm/min
		preamble.append('G92 Z%d; Set the current z as the upper height'%self.options.zhop_height)
		root = effect.document.getroot()
		# Get doc width and height in mm
		self.width = self.svg.uutounit(self.svg.unittouu(root.get('width')), 'mm')
		self.height = self.svg.uutounit(self.svg.unittouu(root.get('height')), 'mm')
		if debug: inkex.debug('Document width:%fmm height:%fmm'%(self.width, self.height))
		# Get transform from user units to mm from viewBox
		svg_transform=element_viewbox_transform(root, self.width, self.height)
		if debug: inkex.debug('viewBox transform: '+transform_str(svg_transform))
		self.recurse_group(root, svg_transform)

	def save(self, stream):
		stream.write(('\n'.join(preamble)+'\n; End preamble\n').encode('utf-8'))
		stream.write(('\n'.join(self.gcode)+'\n; Start postamble\n').encode('utf-8'))
		stream.write(('\n'.join(postamble)+'\n').encode('utf-8'))


#-----------------------------------------------------------------------
if __name__ == '__main__':
	effect = SvgPlotterEffect()
	effect.run()
