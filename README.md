# Inkscape svgplotter Extension

- Version 1.0

This extension creates Gcode suitable for 3D printers fitted with a pen, from Inkscape documents.

It is much, _much_, MUCH easier to use than GcodeTools.

Because Inkscape supports MacOS and linux as well as Windows, it provides a good solution for driving these
printers from all operating systems.

### Liability

By installing and using the svgplotter extension, you accept all responsibility
for any damage or harm that may occur, and exclude the author(s) of
this software from liability for any damages whatsoever.

### Requirements

- Your printer must be fitted with a pen instead of a print head.
- You need [Inkscape](https://inkscape.org/).

### Installing the svgplotter Extension

1. Copy `svgplotter.py` and `svgplotter.inx` into the extensions directory:
   - For Windows `C:\Program Files\Inkscape\share\extensions\`
   - For Mac `~/.config/inkscape/extensions/`
   - For Linux `/usr/share/inkscape/extensions`
2. (Re)start Inkscape.
3. Choose _File/Save a Copy..._ and confirm the file type dropdown contains _Pen plotter gcode file (\*.gcode)_.

### Using svgplotter

It is important to understand two things:

1. **Only path objects are rendered to gcode.** Everything else (text, boxes,
   shapes etc.) is ignored. Luckily Inkscape has a very easy way to convert
   these other objects to paths (_Path/Object to Path_).
2. **Path segments outside the document dimensions are ignored.** So setting
   the Inkscape document width and height to be equal or less than the build
   plate means you will never drive the print head into
   the frame, which could damage the servo motors and driver circuits.

OK, let's make a gcode file:

- Create a new Inkscape document.
- Set the document properties _File/Document Properties_:
  _ Custom size width: How far your printer can travel sideways.
  _ Custom size height: How far your printer can travel up and down.
- Draw something.
- Convert all objects in your document to paths, by selecting them and choosing _Path/Object to Path_.
- Choose _File/Save a Copy..._, select _Pen plotter gcode file (\*.gcode)_ and click _Save_.
- Set the zHop height (the difference between the pen being up or down), set the speed, and click _OK_.
- Adjust your printer's Z height so that the pen is _just above_ the paper (within the zHop height).
- Use something like [Universal Gcode Sender](https://github.com/winder/Universal-G-Code-Sender)
  to send the gcode file to your printer.

### Problems?

If svgplotter does not do what you expect, first check the two points at
the start of the _Using svgplotter_ section.

If this doesn't help, go to [the svgplotter issue tracker](https://bitbucket.org/vig/svgplotter/issues)
and create an issue. You will get much better help if you include:

- A detailed explanation of the problem.
- A detailed explanation of how to re-create the problem.
- The SVG file.

If you know any Python, you can hack the `svgplotter.py` file to try and
fix the problem. Just restart Inkscape each time you make a change.
